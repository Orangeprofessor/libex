/*********************************************************
*
*  Copyright (C) 2014 by Vitaliy Vitsentiy
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*********************************************************/


#ifndef __ctpl_stl_thread_pool_H__
#define __ctpl_stl_thread_pool_H__

#include "../libex.h"

#include <functional>
#include <thread>
#include <atomic>
#include <vector>
#include <memory>
#include <exception>
#include <future>
#include <mutex>
#include <queue>



// thread pool to run user's functors with signature
//      ret func(int id, other_params)
// where id is the index of the thread that runs the functor
// ret is some return type


namespace toolkit
{
	namespace threads
	{
		namespace detail
		{
			template <typename T>
			class Queue
			{
			public:
				bool push(T const & value)
				{
					std::unique_lock<std::mutex> lock(mutex);
					q.push(value);
					return true;
				}
				// deletes the retrieved element, do not use for non integral types
				bool pop(T & v)
				{
					std::unique_lock<std::mutex> lock(mutex);
					if (q.empty())
						return false;
					v = q.front();
					q.pop();
					return true;
				}
				bool empty()
				{
					std::unique_lock<std::mutex> lock(mutex);
					return q.empty();
				}
			private:
				std::queue<T> q;
				std::mutex mutex;
			};
		}

		class CThreadPool
		{
		public:
			CThreadPool();
			CThreadPool(int nThreads);

			// the destructor waits for all the functions in the queue to be finished
			~CThreadPool();

			// get the number of running threads in the pool
			inline int size() { return static_cast<int>(threads.size()); }

			// number of idle threads
			inline int numIdle() { return nWaiting; }
			inline std::thread & getThread(int i) { return *threads[i]; }

			// change the number of threads in the pool
			// should be called from one thread, otherwise be careful to not interleave, also with this->stop()
			// nThreads must be >= 0
			void resize(int nThreads);

			// empty the queue
			void clear();

			// pops a functional wrapper to the original function
			std::function<void(int)> pop();

			// wait for all computing threads to finish and stop all threads
			// may be called asynchronously to not pause the calling thread while waiting
			// if isWait == true, all the functions in the queue are run, otherwise the queue is cleared without running the functions
			void stop(bool isWait = false);

			template<typename F, typename... Rest>
			auto push(F && f, Rest&&... rest)->std::future<decltype(f(0, rest...))>
			{
				auto pck = std::make_shared<std::packaged_task<decltype(f(0))(int)>>(std::forward<F>(f));
				auto _f = new std::function<void(int id)>([pck](int id) {
					(*pck)(id);
				});
				q.push(_f);
				std::unique_lock<std::mutex> lock(this->mutex);
				cv.notify_one();
				return pck->get_future();
			}

			// run the user's function that excepts argument int - id of the running thread. returned value is templatized
			// operator returns std::future, where the user can get the result and rethrow the catched exceptins
			template<typename F>
			auto push(F && f)->std::future<decltype(f(0))>
			{
				auto pck = std::make_shared<std::packaged_task<decltype(f(0))(int)>>(std::forward<F>(f));
				auto _f = new std::function<void(int id)>([pck](int id) {
					(*pck)(id);
				});
				q.push(_f);
				std::unique_lock<std::mutex> lock(this->mutex);
				cv.notify_one();
				return pck->get_future();
			}
		private:
			// deleted
			CThreadPool(const CThreadPool &);// = delete;
			CThreadPool(CThreadPool &&);// = delete;
			CThreadPool & operator=(const CThreadPool &);// = delete;
			CThreadPool & operator=(CThreadPool &&);// = delete;

			void setThread(int i);

			inline void init() { nWaiting = 0; isStop = false; isDone = false; }

			std::vector<std::unique_ptr<std::thread>> threads;
			std::vector<std::shared_ptr<std::atomic<bool>>> flags;
			detail::Queue<std::function<void(int id)> *> q;
			std::atomic<bool> isDone;
			std::atomic<bool> isStop;
			std::atomic<int> nWaiting;  // how many threads are waiting

			std::mutex mutex;
			std::condition_variable cv;
		};
	}
}
#endif // __ctpl_stl_thread_pool_H__
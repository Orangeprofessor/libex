#ifndef HGUARD_LIBEX_TOOLKIT_TIMER
#define HGUARD_LIBEX_TOOLKIT_TIMER
#pragma once

#include "../libex.h"
#include <algorithm>
#include <chrono>

namespace toolkit
{
	class Timer
	{
		using clock_t = std::chrono::time_point<std::chrono::steady_clock>;

	public:
		Timer(void) = default;

		/**
		* @fn  explicit Timer( const uint64_t duration );
		*
		*     @brief   Constructor.
		*
		*     @Author  ReactiioN
		*     @date    25.10.2016
		*
		*     @Param   duration    The duration.
		*/
		explicit Timer(const uint64_t duration);

		/**
		* @fn  bool is_finished( void ) const;
		*
		*     @brief   Query if this object is finished.
		*
		*     @Author  ReactiioN
		*     @date    25.10.2016
		*
		*     @returN  true if finished, false if not.
		*/
		bool            is_finished(void) const;

		/**
		* @fn  bool run( void );
		*
		*     @brief   Runs this object.
		*
		*     @Author  ReactiioN
		*     @date    25.10.2016
		*
		*     @returN  true if it succeeds, false if it fails.
		*/
		bool            run(void);

		/**
		* @fn  void reset( void );
		*
		*     @brief   Resets this object.
		*
		*     @Author  ReactiioN
		*     @date    25.10.2016
		*/
		void            reset(void);

		/**
		* @fn  void set_duration( const uint64_t duration );
		*
		*     @brief   Sets a duration.
		*
		*     @Author  ReactiioN
		*     @date    25.10.2016
		*
		*     @Param   duration    The duration.
		*/
		void            set_duration(const uint64_t duration);

		/**
		* @fn  template<typename T = double> T get_percentage( void ) const;
		*
		*     @brief   Gets the percentage.
		*
		*     @Author  ReactiioN
		*     @date    25.10.2016
		*
		* @tparam  T   Generic type parameter.
		*
		*     @returN  The percentage.
		*/
		template<typename T = double>
		T               get_percentage(void) const;

	private:
		/**
		* @fn  static clock_t get_current_time( void );
		*
		*     @brief   Gets current time.
		*
		*     @Author  ReactiioN
		*     @date    25.10.2016
		*
		*     @returN  The current time.
		*/
		static clock_t  get_current_time(void);

		/**
		* @fn  static uint64_t get_time_diff( const clock_t& init_time );
		*
		*     @brief   Gets time difference.
		*
		*     @Author  ReactiioN
		*     @date    25.10.2016
		*
		*     @Param   init_time   The init time.
		*
		*     @returN  The time difference.
		*/
		static uint64_t get_time_diff(const clock_t& init_time);

	private:
		bool     m_Started = false;
		bool     m_Finished = false;
		clock_t  m_Start;
		uint64_t m_Duration = 0;
	};

	template<typename T>
	T Timer::get_percentage(void) const
	{
		static_assert(std::is_arithmetic<T>::value, "Type T has to be arithmetic");
		if (!m_Started) {
			return static_cast<T>(0.0);
		}
		return static_cast<T>((std::min)(1.0, static_cast<double>(get_time_diff(m_Start)) / static_cast<double>(m_Duration)));
	}
}

#endif

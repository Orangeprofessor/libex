
#include "threads.hpp"

namespace toolkit
{
	namespace threads
	{
		CThreadPool::CThreadPool() { init(); }

		CThreadPool::CThreadPool(int nThreads) { init(); resize(nThreads); }

		CThreadPool::~CThreadPool() { stop(true); }

		void CThreadPool::resize(int nThreads)
		{
			if (!isStop && !isDone) 
			{
				int oldNThreads = static_cast<int>(threads.size());
				if (oldNThreads <= nThreads)
				{  // if the number of threads is increased
					threads.resize(nThreads);
					flags.resize(nThreads);

					for (int i = oldNThreads; i < nThreads; ++i) 
					{
						flags[i] = std::make_shared<std::atomic<bool>>(false);
						setThread(i);
					}
				}
				else 
				{  // the number of threads is decreased
					for (int i = oldNThreads - 1; i >= nThreads; --i) 
					{
						*flags[i] = true;  // this thread will finish
						threads[i]->detach();
					}
					{
						// stop the detached threads that were waiting
						std::unique_lock<std::mutex> lock(this->mutex);
						cv.notify_all();
					}
					threads.resize(nThreads);  // safe to delete because the threads are detached
					flags.resize(nThreads);  // safe to delete because the threads have copies of shared_ptr of the flags, not originals
				}
			}
		}

		void CThreadPool::clear()
		{
			std::function<void(int id)> * _f;
			while (q.pop(_f))
				delete _f; // empty the queue
		}

		std::function<void(int)> CThreadPool::pop()
		{
			std::function<void(int id)> * _f = nullptr;
			q.pop(_f);
			std::unique_ptr<std::function<void(int id)>> func(_f); // at return, delete the function even if an exception occurred
			std::function<void(int)> f;
			if (_f)
				f = *_f;
			return f;
		}

		void CThreadPool::stop(bool isWait)
		{
			if (!isWait)
			{
				if (isStop)
					return;
				isStop = true;
				for (int i = 0, n = size(); i < n; ++i) {
					*flags[i] = true;  // command the threads to stop
				}
				clear();  // empty the queue
			}
			else 
			{
				if (isDone || isStop)
					return;
				isDone = true;  // give the waiting threads a command to finish
			}
			{
				std::unique_lock<std::mutex> lock(mutex);
				cv.notify_all();  // stop all waiting threads
			}
			for (int i = 0; i < static_cast<int>(threads.size()); ++i) {
				// wait for the computing threads to finish
				if (threads[i]->joinable())
					threads[i]->join();
			}
			// if there were no threads in the pool but some functors in the queue, the functors are not deleted by the threads
			// therefore delete them here
			clear();
			threads.clear();
			flags.clear();
		}

		void CThreadPool::setThread(int i)
		{
			std::shared_ptr<std::atomic<bool>> flag(flags[i]); // a copy of the shared ptr to the flag
			auto f = [this, i, flag/* a copy of the shared ptr to the flag */]() 
			{
				std::atomic<bool> & _flag = *flag;
				std::function<void(int id)> * _f;
				bool isPop = this->q.pop(_f);
				while (true) 
				{
					while (isPop) 
					{  // if there is anything in the queue
						std::unique_ptr<std::function<void(int id)>> func(_f); // at return, delete the function even if an exception occurred
						(*_f)(i);
						if (_flag)
							return;  // the thread is wanted to stop, return even if the queue is not empty yet
						else
							isPop = q.pop(_f);
					}
					// the queue is empty here, wait for the next command
					std::unique_lock<std::mutex> lock(this->mutex);
					++nWaiting;
					cv.wait(lock, [this, &_f, &isPop, &_flag]() { isPop = q.pop(_f); return isPop || isDone || _flag; });
					--nWaiting;
					if (!isPop)
						return;  // if the queue is empty and this->isDone == true or *flag then return
				}
			};
			threads[i].reset(new std::thread(f)); // compiler may not support std::make_unique()
		}
	}
}
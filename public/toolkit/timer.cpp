#include "timer.h"

namespace toolkit
{
	Timer::Timer(const uint64_t duration) :
		m_Duration(duration)
	{
	}

	bool Timer::is_finished(void) const
	{
		return m_Finished;
	}

	bool Timer::run(void)
	{
		if (m_Finished) {
			return true;
		}
		if (!m_Started) {
			m_Start = get_current_time();
			m_Started = true;
		}

		m_Finished = get_time_diff(m_Start) >= m_Duration;
		return m_Finished;
	}

	void Timer::reset(void)
	{
		m_Finished = false;
		m_Started = false;
	}

	void Timer::set_duration(const uint64_t duration)
	{
		m_Duration = duration;
	}

	Timer::clock_t Timer::get_current_time(void)
	{
		return std::chrono::high_resolution_clock::now();
	}

	uint64_t Timer::get_time_diff(const clock_t& init_time)
	{
		return std::chrono::duration_cast< std::chrono::milliseconds >(get_current_time() - init_time).count();
	}
}
#ifndef HGUARD_LIBEX_HASH_HASHV3
#define HGUARD_LIBEX_HASH_HASHV3
#pragma once

//------------------------------------------------
// Basic string hashing
//------------------------------------------------
// Based on www.cse.yorku.ca/~oz/hash.html

#include "../libex.h"

//
// Has to be forced inline or these might fail
//
#define inline INLINE

//
// Generate a hash at compile time
// Uses macros, see make_cthash.bat for the generator
// Important! These *have* to be force inlined or they will fail under msvc!
//
#define HASH(S) _cthash<sizeof(S)/sizeof(S[0])>(S)

namespace hash
{
//
// Actual implementation of the specific hashes
//

struct hash_t
{
	typedef unsigned int type;
	enum { init = 5381 };
	static inline type algo( type h, type c )
	{
		return (h*33)^c;
	}
};
struct djb2_t
{
	typedef unsigned int type;
	enum { init = 5381 };
	static inline type algo( type h, type c )
	{
		return (h*33)+c;
	}
};
struct sdbm_t
{
	typedef unsigned int type;
	enum { init = 0 };
	static inline type algo( type h, type c )
	{
		return c + (h<<6) + (h<<16) - h;
	}
};
struct lose_t
{
	typedef unsigned int type;
	enum { init = 0 };
	static inline type algo( type h, type c )
	{
		return h + c;
	}
};


//
// OOP version, specify hash algorithm and character type
//
template< typename H, typename T >
class hashit_t
{
public:
	typedef typename H::type type;
	typedef typename hashit_t<H,T> this_t;
	inline explicit hashit_t( type h = H::init ) : value(h) { }
	inline hashit_t( const this_t& other ) : value(other.value) { }
	inline hashit_t( const T* str, type init = H::init ) : value( init ) { hash( str ); }
	inline hashit_t( const T* it, const T* end, type init = H::init ) : value( init ) { hash( it, end ); }

	// Append additional character
	inline this_t& hash( T c )
	{
		value = H::algo( value, static_cast<types::make_unsigned<T>::type>(c) );
		return *this;
	}
	// Append a zero-terminated string
	this_t& hash( const T* str )
	{
		for ( T c; c = *str; ++str )
			hash( c );
		return *this;
	}
	// Append from iterator
	this_t& hash( const T* it, const T* end )
	{
		for ( ; it<end; ++it )
			hash( *it );
		return *this;
	}
	// Append number of characters from string
	inline this_t& hash( const T* str, unsigned int count )
	{
		return hash( str, str+count );
	}
	inline operator type () const { return value; }
private:
	type value;
};

//
// Functional version
//
template< typename H, typename T >
inline typename H::type hashit( const T* str, typename H::type h = H::init )
{
	return hashit_t<H,T>( str, h );
}
template< typename H, typename T >
inline typename H::type hashit( const T* it, const T* end, typename H::type h = H::init )
{
	return hashit_t<H,T>( it, end, h );
}
template< typename H, typename T >
inline typename H::type hashit( T c, typename H::type h = H::init )
{
	return H::algo( h, static_cast<types::make_unsigned<T>::type>(c) );
}

//
// Legacy and convenience support
//
template< typename T >
inline hash_t::type hash( const T* str, hash_t::type h = hash_t::init )
{
	return hashit<hash_t,T>( str, h );
}
template< typename T >
inline hash_t::type hash( const T* it, const T* end, hash_t::type h = hash_t::init )
{
	return hashit<hash_t,T>( it, end, h );
}

}


//------------------------------------------------
// Compile time hashes generation
//------------------------------------------------
//
// Can hash strings up to a length of 110 characters.
// Compatible only with hash::hash_t.
// WARNING! May screw up occasionally under msvc!
//

template< unsigned int L > inline unsigned int _cthash( const wchar_t (&str)[L] );
template< unsigned int L > inline unsigned int _cthash( const char (&str)[L] );

#define CTHASHIMPL(I,T,F) template<> inline unsigned int _cthash<I>( const T (&str)[I] ) { return F(str); }
#define CTHASHOP(H,S,I) (((H)*33)^EXT(S[I]))
#define CTHASH0(S) 5381

#include "_cthash.h"

#undef CTHASHIMPL
#undef CTHASHOP
#undef CTHASH0

#undef inline


#endif // !HGUARD_LIBEX_HASH_HASHV3

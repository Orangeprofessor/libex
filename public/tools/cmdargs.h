#pragma once

#include "..\hash\idtype.h"

namespace tools
{

/// Simple command line string parsing.
template< typename Char >
class CCmdArgs
{
	/// Prevent copy construction.
	CCmdArgs( const CCmdArgs& );

public:
	typedef hash::IdType<Char> IdType;

	/// Default constructor, zero initializes its members.
	CCmdArgs();

	/// Destructor to clean up memory.
	~CCmdArgs();

	/// Construct and immediately parse a string.
	/// \param str Command line string to parse.
	/// \param end Specifies the end if the string is not zero-terminated.
	explicit CCmdArgs( const Char* str, const Char* end = nullptr );

	/// Construct without parsing. Some features not available.
	/// \param argc Number of arguments in second parameter.
	/// \param argv Array of pointers to each argument.
	CCmdArgs( int argc, Char* argv[] );

	/// Parse a new command line string; errors are ignored, doesn't throw or anything.
	/// \param str Command line string to parse.
	/// \param end Specifies the end if the string is not zero-terminated.
	void Parse( const Char* str, const Char* end = nullptr );

	/// Get the total number of arguments.
	/// \return Number of arguments, including the 'command'.
	unsigned int Count() const;

	/// Get an argument by index.
	const Char* Get( unsigned int i ) const;

	/// Get the remaining arguments starting from specified index.
	/// The result is exactly as the user typed starting from specified argument (including quotes and extra spaces).
	const Char* Gets( unsigned int i = 1 ) const;

	/// Operator convenience.
	const Char* operator[] ( unsigned int i ) const;

	/// Gets the single argument.
	/// When expecting a single argument it should be quoted if it has spaces, but sometimes it's more convenient to lump all the arguments together without quotes.
	/// \return If there's exactly one argument, returns that argument specifically (no quotes), else it returns all the arguments.
	const Char* SingleArg() const;

	/// Finds the specified option switch by name.
	/// Assumed structure of the command line is: CMD OPTS ARGS.
	///
	/// Options can have a short and verbose form;
	///
	/// * Short options start with a single dash and a single character followed directly by the argument; example: `-Oargument` where `shorthand` = `"O"`, returns `"argument"`.
	/// * Verbose options start with two dashes followed by any number of characters, argument is optional but must follow the equality sign; Example: `--verbose=argument` where `verbose` = `"verbose"`, returns `"argument"`.
	/// * If both a shorthand and verbose options are used, the first one will be returned.
	///
	/// Arguments start at the first arg that doesn't start with a dash or after --.
	///
	/// \param shorthand One hypen and a single character option, zero indicates no shorthand available.
	/// \param verbose Two hypens and the option, zero indicates no verbose available.
	/// \return Returns nullptr if not found, empty string if no argument specified, otherwise the argument string.
	const Char* Option( IdType shorthand, IdType verbose ) const;

	/// Iterator type is a simple pointer, only support const.
	typedef const Char** const_iterator;
	/// Iteration starts at the first non-option argument.
	const_iterator begin() const;
	/// Iteration starts at the first non-option argument.
	const_iterator end() const;

protected:
	static bool _isspace( Char c );

private:
	unsigned int _argc;
	Char** _argv;
	const Char** _tags;
};

}

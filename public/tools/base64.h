
namespace tools
{

// Given the length of the data to be encoded, returns the textual length (without zero terminator)
inline unsigned int base64_enclen( unsigned int size ) { return (size + 2) / 3 * 4; }
// Encodes the input buffer with given size to output (including zero terminator)
void base64_encode( const unsigned char* input, unsigned int size, char* output );

template< typename T >
inline void base64_encode( const T* input, char* output )
{
	base64_encode( (const unsigned char*)input, sizeof(T), output );
}

// Given the length of the text to be decoded, returns the 
inline unsigned int base64_declen( const char* begin, const char* end ) { return (end - begin) / 4 * 3; }
// Decodes the text buffer to output
bool base64_decode( const char* begin, const char* end, unsigned char* output );

template< typename T >
inline bool base64_decode( const char* begin, const char* end, T* output )
{
	return ( base64_declen( begin, end )==sizeof(T) ) ? base64_decode( begin, end, (unsigned char*)output ) : false;
}

}

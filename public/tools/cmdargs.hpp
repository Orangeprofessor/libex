#pragma once

#include "cmdargs.h"

#include <cstring>

namespace tools
{


template< typename Char >
CCmdArgs<Char>::CCmdArgs() :
	_argc(0),
	_argv(nullptr),
	_tags(nullptr) {}
template< typename Char >
CCmdArgs<Char>::~CCmdArgs()
{
	// Everthing is allocated in a single malloc.
	free( _argv );
}
template< typename Char >
CCmdArgs<Char>::CCmdArgs( const Char* begin, const Char* end ) :
	_argv(nullptr)
{
	Parse( begin, end );
}
template< typename Char >
CCmdArgs<Char>::CCmdArgs( int argc, Char* argv[] ) :
	_argc(static_cast<unsigned int>(argc)),
	_argv(argv),
	_tags(nullptr) {}


template< typename Char >
bool CCmdArgs<Char>::_isspace( Char c )
{
	return c==' ' || c=='\t' || c=='\n' || c=='\r';
}

template< typename Char >
void CCmdArgs<Char>::Parse( const Char* begin, const Char* end )
{
	// Step one: Free any existing memory

	free( _argv );

	// Step two: Figure out how many arguments we got

	if ( !end ) {
		end = begin + strlen(begin);
	}

	_argc = 0;
	Char quote = 0;
	bool space = true;

	for ( auto it = begin; it<end; ++it ) {
		Char c = *it;

		if ( quote ) {
			if ( c==quote ) {
				// Found the closing quote.
				quote = 0;
			}
		}
		else if ( _isspace( c ) ) {
			space = true;
		}
		else {
			if ( space ) {
				// Previous char was a space
				++_argc;
			}
			space = false;

			// Start of a quoted sequence
			if ( c=='\'' || c=='\"' ) {
				quote = c;
			}
		}
	}

	// Step three: Short circuit if no arguments found

	if ( !_argc ) {
		_argv = nullptr;
		_tags = nullptr;
		return;
	}

	// Step four: Allocate memory

	void* p = malloc( _argc*sizeof(const Char**)*2 + (end-begin+1)*sizeof(Char)*2 );

	_argv = (Char**)p;
	_tags = (const Char**)_argv + _argc;

	Char* as = (Char*)( _tags + _argc );
	Char* ts = as + (end-begin+1);

	// Step five: Start by copying the tags string
	// FIXME! This may be unnecessary, we can just commandeer the string given to us...

	memcpy( ts, begin, (end-begin)*sizeof(Char) );
	ts[end-begin] = 0;

	// Step six: Parse the string

	quote = 0;
	space = true;
	unsigned int argi = 0;

	for ( auto it = begin; it<end; ++it ) {
		Char c = *it;

		if ( quote ) {
			if ( c==quote ) {
				// Found the closing quote.
				quote = 0;
			}
			else {
				*as++ = c;
			}
		}
		else if ( _isspace( c ) ) {
			// Check if previous argument is over and zero terminate it.
			if ( !space && argi>0 ) {
				*as++ = 0;
			}
			space = true;
		}
		else {
			if ( c=='\'' || c=='\"' ) {
				quote = c;
				if ( space ) {
					// Start of a new argument that did start with a quote
					_argv[argi] = as;
					_tags[argi] = ts + (it-begin);
					++argi;
					space = false;
				}
			}
			else if ( space ) {
				// Start of a new argument NOT starting with a quote
				_argv[argi] = as;
				_tags[argi] = ts + (it-begin);
				++argi;
				*as++ = c;
				space = false;
			}
			else {
				*as++ = c;
			}
		}
	}
	*as = 0;
}



template< typename Char >
unsigned int CCmdArgs<Char>::Count() const
{
	return _argc;
}
template< typename Char >
const Char* CCmdArgs<Char>::Get( unsigned int i ) const
{
	assert( _argv && i<_argc );
	return _argv[i];
}
template< typename Char >
const Char* CCmdArgs<Char>::Gets( unsigned int i = 1 ) const
{
	assert( _tags && i<_argc );
	return _tags[i];
}
template< typename Char >
const Char* CCmdArgs<Char>::operator[] ( unsigned int i ) const
{
	return Get( i );
}
template< typename Char >
const Char* CCmdArgs<Char>::SingleArg() const
{
	return ( Count()==2 ) ? Get( 1 ) : Gets( 1 );
}



template< typename Char >
const Char* CCmdArgs<Char>::Option( IdType shorthand, IdType verbose ) const
{
	typedef IdType::hash_t hash_t;

	for ( auto it = _argv+1, end = _argv+_argc; it<end; ++it ) {
		auto arg = *it;
		if ( arg[0]=='-' ) {
			if ( arg[1]=='-' ) {
				// Stop looking for options when we found --
				if ( arg[2]==0 ) {
					break;
				}
				else if ( verbose ) {
					// Verbose option
					hash_t::type h = hash_t::init;
					auto jt = arg+2;
					for ( ; *jt && *jt!='='; ++jt ) {
						h = hash::hashit<hash_t>( *jt, h );
					}
					if ( h==verbose ) {
						return jt + (*jt=='=');
					}
				}
			}
			else if ( shorthand ) {
				// Shorthand option
				if ( hash::hashit<hash_t>( arg[1] )==shorthand ) {
					return arg+2;
				}
			}
		}
		else {
			// Found a non-option, no more options will follow.
			break;
		}
	}
	return nullptr;
}



template< typename Char >
typename CCmdArgs<Char>::const_iterator CCmdArgs<Char>::begin() const
{
	// Find first non-option argument
	auto it = _argv+1;
	for ( auto end = _argv+_argc; it<end && *it[0]!='-'; ++it ) {}
	return it;
}
template< typename Char >
typename CCmdArgs<Char>::const_iterator CCmdArgs<Char>::end() const
{
	return _argv+_argc;
}

}
